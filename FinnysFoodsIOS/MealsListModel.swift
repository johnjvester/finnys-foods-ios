//
//  MealsListModel.swift
//  FinnysFoodsIOS
//
//  Created by John Vester on 11/7/21.
//  Copyright © 2021 FinnysFoodsIOSOrganizationName. All rights reserved.
//

import Combine
import SmartStore
import MobileSync

struct Meal: Hashable, Identifiable, Decodable {
    var id: String
    let Name: String
    let Rating__c: Double
}

struct MealResponse: Decodable {
    var totalSize: Int
    var done: Bool
    var records: [Meal]
}

class MealsListModel: ObservableObject {
    
    @Published var meals: [Meal] = []
    
    var store: SmartStore?
    var syncManager: SyncManager?
    private var syncTaskCancellable: AnyCancellable?
    private var storeTaskCancellable: AnyCancellable?
    
    init() {
         store = SmartStore.shared(withName: SmartStore.defaultStoreName)
         syncManager = SyncManager.sharedInstance(store: store!)
    }
     
    func fetchMeals(){
         syncTaskCancellable = syncManager?.publisher(for: "syncDownMeals")
             .receive(on: RunLoop.main)
             .sink(receiveCompletion: { _ in }, receiveValue: { _ in
                 self.loadFromSmartStore()
         })
         self.loadFromSmartStore()
     }
     
     private func loadFromSmartStore() {
          storeTaskCancellable = self.store?.publisher(for: "select {Meal__c:Id}, {Meal__c:Name}, {Meal__c:Rating__c} from {Meal__c}")
             .receive(on: RunLoop.main)
             .tryMap {
                 $0.map { (row) -> Meal in
                     let r = row as! [String?]
                     return Meal(id: r[0] ?? "", Name: r[1] ?? "", Rating__c: (r[2]! as NSString).doubleValue)
                 }
             }
             .catch { error -> Just<[Meal]> in
                 print(error)
                 return Just([Meal]())
             }
             .assign(to: \MealsListModel.meals, on:self)
     }
}
