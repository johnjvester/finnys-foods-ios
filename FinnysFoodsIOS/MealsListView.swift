//
//  MealsListView.swift
//  FinnysFoodsIOS
//
//  Created by John Vester on 11/7/21.
//  Copyright © 2021 FinnysFoodsIOSOrganizationName. All rights reserved.
//

import Foundation
import SwiftUI
import Combine
import SalesforceSDKCore
import FASwiftUI

struct MealsListView: View {
  @ObservedObject var viewModel = MealsListModel()
  
  var body: some View {
    Text("Finny's Foods (iOS)").bold()
    List(viewModel.meals) { dataItem in
      HStack(spacing: 10) {
        VStack(alignment: .leading, spacing: 3) {
            HStack {
                Text(dataItem.Name)
                Text(" ")
                
                switch dataItem.Rating__c {
                    case 5:
                        FAText(iconName: "star", size: 15, style: .solid)
                        FAText(iconName: "star", size: 15, style: .solid)
                        FAText(iconName: "star", size: 15, style: .solid)
                        FAText(iconName: "star", size: 15, style: .solid)
                        FAText(iconName: "star", size: 15, style: .solid)
                    case 4:
                        FAText(iconName: "star", size: 15, style: .solid)
                        FAText(iconName: "star", size: 15, style: .solid)
                        FAText(iconName: "star", size: 15, style: .solid)
                        FAText(iconName: "star", size: 15, style: .solid)
                        FAText(iconName: "star", size: 15)
                    case 3:
                        FAText(iconName: "star", size: 15, style: .solid)
                        FAText(iconName: "star", size: 15, style: .solid)
                        FAText(iconName: "star", size: 15, style: .solid)
                        FAText(iconName: "star", size: 15)
                        FAText(iconName: "star", size: 15)
                    case 2:
                        FAText(iconName: "star", size: 15, style: .solid)
                        FAText(iconName: "star", size: 15, style: .solid)
                        FAText(iconName: "star", size: 15)
                        FAText(iconName: "star", size: 15)
                        FAText(iconName: "star", size: 15)
                    default:
                        FAText(iconName: "star", size: 15, style: .solid)
                        FAText(iconName: "star", size: 15)
                        FAText(iconName: "star", size: 15)
                        FAText(iconName: "star", size: 15)
                        FAText(iconName: "star", size: 15)
                }
            }
        }
      }
    }
    .onAppear{ self.viewModel.fetchMeals()}
  }
}

struct MealsList_Previews: PreviewProvider {
  static var previews: some View {
    MealsListView()
  }
}
